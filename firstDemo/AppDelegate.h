//
//  AppDelegate.h
//  firstDemo
//
//  Created by YAN on 16/5/8.
//  Copyright © 2016年 Minstone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

